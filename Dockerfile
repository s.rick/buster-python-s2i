FROM python:3.7-slim-buster

ENV SUMMARY="Base python slim buster image which allows the use of s2i" \
    DESCRIPTION="s2i image building on slim buster python. This image tries to be as small als possible while providing all for s2i needed requirements" \
    NAME="slim-buster-python-s2i"

LABEL summary=$SUMMARY \
      description=$DESCRIPTION \
      io.k8s.description=$DESCRIPTION \
      io.k8s.display-name=$NAME \
      # labels for s2i and openshift where to find the s2i scripts
      io.openshift.s2i.scripts-url=image:///usr/libexec/s2i \
      io.s2i.scripts-url=image:///usr/libexec/s2i \
      name=$NAME \
      version="0"

ENV APP_ROOT=/opt/app-root \
    HOME=/opt/app-root/src \
    # extend path with home directory
    PATH=$HOME/.local/bin:$HOME/bin:$APP_ROOT/bin:$PATH \
    PIP_NO_CACHE_DIR=1 \
    # set language
    LANGUAGE=de_DE.utf-8 \
    LANG=de_DE.utf-8 \
    LC_ALL=de_DE.utf-8 \
    # DEPRECATED: Use above labels instead
    STI_SCRIPTS_URL=image:///usr/libexec/s2i 

# activate venv on startup
ENV BASH_ENV="${APP_ROOT}/bin/activate" \
    ENV="${APP_ROOT}/bin/activate" \
    PROMPT_COMMAND=". ${APP_ROOT}/bin/activate"

# copy s2i basic files and utility scripts
COPY ./root/ /

WORKDIR ${HOME}
ENTRYPOINT ["container-entrypoint"]
CMD ["base-usage"]

# create user openshift uses while s2i, make the app_root his and change utitilties access
RUN adduser --system --uid 1001  --gid 0 --home ${HOME} --shell /sbin/nologin default; \
    python -m venv ${APP_ROOT}; \
    chown -R 1001:0 ${APP_ROOT}; \
    chmod 777 /usr/libexec/s2i/*; \
    chmod 777 /usr/bin/*; \
    fix-permissions ${APP_ROOT}